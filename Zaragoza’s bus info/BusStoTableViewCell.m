//
//  BusStoTableViewCell.m
//  Zaragoza’s bus info
//
//  Created by Rene Wetzlar on 31.07.16.
//  Copyright © 2016 renewetzlar. All rights reserved.
//

#import "BusStoTableViewCell.h"

@implementation BusStoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
