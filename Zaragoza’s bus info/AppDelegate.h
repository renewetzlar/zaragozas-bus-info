//
//  AppDelegate.h
//  Zaragoza’s bus info
//
//  Created by Rene Wetzlar on 31.07.16.
//  Copyright © 2016 renewetzlar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

