//
//  BusStoTableViewCell.h
//  Zaragoza’s bus info
//
//  Created by Rene Wetzlar on 31.07.16.
//  Copyright © 2016 renewetzlar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusStoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;
@property (weak, nonatomic) IBOutlet UILabel *arrivalTimeLabel;

@end
