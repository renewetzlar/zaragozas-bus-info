//
//  BusStop.h
//  Zaragoza’s bus info
//
//  Created by Rene Wetzlar on 31.07.16.
//  Copyright © 2016 renewetzlar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusStop : NSObject

@property int busStopId;
@property NSString *title;
@property NSString *subtitle;
@property NSString *mapImagePreviewURL;
@property NSString *arrivingTime;

@end
