//
//  ViewController.m
//  Zaragoza’s bus info
//
//  Created by Rene Wetzlar on 31.07.16.
//  Copyright © 2016 renewetzlar. All rights reserved.
//

#import "ViewController.h"
#import "BusStoTableViewCell.h"
#import "BusStop.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSArray *busStops;

@property dispatch_semaphore_t semaphore;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    
    [self loadBusStops];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark Data Source Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.busStops count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"busStopCell";
    BusStoTableViewCell *busStopCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    BusStop *busStop = self.busStops[indexPath.row];
    
    busStopCell.titleLabel.text = busStop.title;
    
    busStopCell.subTitleLabel.text = busStop.subtitle;
    
    [self loadRealtimeInfos:busStopCell busStop:busStop indexPath:indexPath];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *imageUrl = [NSURL URLWithString:[busStop.mapImagePreviewURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        NSData *imgData = [NSData dataWithContentsOfURL:imageUrl];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *mapImage = [UIImage imageWithData:imgData];
            [busStopCell.mapImage setImage:mapImage];
        });
    });
    
    return busStopCell;
}

# pragma mark Helper

- (void) loadBusStops {
    // first request
    NSURL *url = [NSURL URLWithString:[@"http://api.dndzgz.com/services/bus" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlRequest
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if(error == nil) {
                                          
                                          NSError *serializeError = nil;
                                          NSDictionary *jsonData = [NSJSONSerialization
                                                                    JSONObjectWithData:data
                                                                    options:NSJSONReadingMutableContainers
                                                                    error:&serializeError];
                                          
                                          if (serializeError == nil) {
                                              NSArray *locations = [jsonData objectForKey:@"locations"];
                                              NSMutableArray *busStops = [NSMutableArray array];
                                              
                                              for (NSDictionary *location in locations) {
                                                  BusStop *busStop = [[BusStop alloc]init];
                                                  
                                                  NSString *title = [location valueForKey:@"title"];
                                                  if (title != nil) {
                                                      busStop.title = title;
                                                  }
                                                  NSString *subtitle = [location valueForKey:@"subtitle"];
                                                  if (subtitle != nil) {
                                                      busStop.subtitle = subtitle;
                                                  }
                                                  float lat = [[location objectForKey:@"lat"] floatValue];
                                                  float lon = [[location objectForKey:@"lon"] floatValue];
                                                  if (lat != 0 && lon != 0) {
                                                      busStop.mapImagePreviewURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=15&size=150x100&sensor=true", lat, lon];
                                                  }
                                                  int busStopId = [[location objectForKey:@"id"] intValue];
                                                  if (busStopId != 0) {
                                                      busStop.busStopId = busStopId;
                                                  }
                                                  [busStops addObject:busStop];
                                              }
                                              
                                              self.busStops = busStops;
                                              NSLog(@"Data = %@", locations);
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [self.tableView reloadData];
                                              });
                                          }
                                          
                                      }
                                  }];
    
    [task resume];
}

- (void) loadRealtimeInfos:(BusStoTableViewCell *)busStopTvC busStop:(BusStop *)busStop indexPath:(NSIndexPath *)indexPath {
    
    NSString *urlString = [NSString stringWithFormat:@"http://api.dndzgz.com/services/bus/%i", busStop.busStopId];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlRequest
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error == nil) {
                                          NSError *serializeError = nil;
                                          NSDictionary *jsonData = [NSJSONSerialization
                                                                    JSONObjectWithData:data
                                                                    options:NSJSONReadingMutableContainers
                                                                    error:&serializeError];
                                          NSLog(@"Json: %@",jsonData);
                                          NSArray *estimates = [jsonData objectForKey:@"estimates"];
                                          if (estimates != nil && [estimates count] > 1) {
                                              NSDictionary *estimateObject =[estimates firstObject];
                                              if (estimateObject != NULL) {
                                                  if ([estimateObject valueForKey:@"estimate"] != [NSNull null]) {
                                                      int estimate = [[estimateObject valueForKey:@"estimate"] intValue];
                                                      NSLog(@"esti %i", estimate);
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          busStop.arrivingTime = [NSString stringWithFormat:@"%i min", estimate];
                                                          busStopTvC.arrivalTimeLabel.text = busStop.arrivingTime;
                                                          [busStopTvC setNeedsDisplay];
                                                      });
                                                  }
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

@end
